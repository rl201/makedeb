# vim: set noexpandtab tabstop=4:
lc = $(subst A,a,$(subst B,b,$(subst C,c,$(subst D,d,$(subst E,e,$(subst F,f,$(subst G,g,$(subst H,h,$(subst I,i,$(subst J,j,$(subst K,k,$(subst L,l,$(subst M,m,$(subst N,n,$(subst O,o,$(subst P,p,$(subst Q,q,$(subst R,r,$(subst S,s,$(subst T,t,$(subst U,u,$(subst V,v,$(subst W,w,$(subst X,x,$(subst Y,y,$(subst Z,z,$1))))))))))))))))))))))))))

TMPDIR:=$(shell mktemp -d)
MAKEDEBDIR:=$(dir $(realpath $(firstword $(MAKEFILE_LIST))))

PKG:=$(shell grep ^Package ROOT/DEBIAN/control | awk '{print $$2}')
VERSION:=$(shell grep ^Version: ROOT/DEBIAN/control | awk ' { print $$2 } ')
ARCH:=$(shell grep ^Architecture: ROOT/DEBIAN/control | awk ' { print $$2 } ')
DISTS:=$(notdir $(wildcard dists/*))
DOWNLOADS:=downloads.ch.private.cam.ac.uk

# Default target: show the help message
default: help

help:
	@echo make skel : create a new outline for a new repository
	@echo make build : build a deb \(NB will autobump the version in the control file if need be\)
	@echo make upload : upload to local deb repo, for all filenames found in dists/
	@echo make gittag : tag the repo with the version from the control file

COMMITSAHEAD:=$(shell git rev-list HEAD@{upstream}..HEAD | wc -l)
UNCOMMITED_CHANGES:=$(shell git status --porcelain | wc -l)
upload: selfcheck $(PKG)_$(VERSION)_$(ARCH).deb $(foreach D,$(DISTS),.upload-$(D)) gitwarning ## upload a deb to local-deb server

# internal usage 
$(PKG)_$(VERSION)_$(ARCH).deb:
	@echo Rebuilding
	@rm -Rf .upload-*
	@$(MAKE) build

build: selfcheck  bumpvers  $(foreach D,$(DISTS),.build-$(D))
build:
	@echo
	@echo Next consider running make upload \(will upload to $(DOWNLOADS) for: $(DISTS)\)

.distdir-%: DIST=$*
.distdir-%:
	@mkdir -p $(DIST)

.build-%: DIST=$*
.build-%: BUILDDIR=$(TMPDIR)/$(DIST)
.build-%: selfcheck ## build a deb but do not upload
	@mkdir -p $(DIST)
	@mkdir -p $(BUILDDIR)
	@(cd ROOT && tar -cf - .) | (cd $(BUILDDIR) && tar -xf -)
	@if [ -d $(DIST)/ROOT ] ; then find $(DIST)/ROOT -type d -exec mkdir -p $(DIST)/build/{} \;  ; fi
	@if [ -d $(DIST)/ROOT ] ; then rsync -a $(DIST)/ROOT/ $(BUILDDIR)/ ; fi
	@cat ROOT/DEBIAN/control >$(BUILDDIR)/DEBIAN/control
	@if [ -f $(BUILDDIR)/DEBIAN/postinst ] ; then chmod 755 $(BUILDDIR)/DEBIAN/postinst ; fi
	@if [ -f $(BUILDDIR)/DEBIAN/preinst ] ; then chmod 755 $(BUILDDIR)/DEBIAN/preinst ; fi
	@if [ -f $(BUILDDIR)/DEBIAN/prerm ] ; then chmod 755 $(BUILDDIR)/DEBIAN/prerm ; fi
	@if [ -f $(BUILDDIR)/DEBIAN/postrm ] ; then chmod 755 $(BUILDDIR)/DEBIAN/postrm ; fi
	@if [ -f $(BUILDDIR)/DEBIAN/config ] ; then chmod 755 $(BUILDDIR)/DEBIAN/config ; fi
	@if [ -f $(BUILDDIR)/DEBIAN/templates ] ; then chmod 755 $(BUILDDIR)/DEBIAN/templates ; fi
	@if [ -f $(BUILDDIR)/DEBIAN/conffiles ] ; then \
		(cd $(BUILDDIR) ; [ -d etc ] && find etc -type f -exec grep -v -q {} DEBIAN/conffiles \; -exec echo {} >> DEBIAN/conffiles \; || true) ; \
	else \
		(cd $(BUILDDIR) ; [ -d etc ] && find etc -type f -exec echo {} >> DEBIAN/conffiles \; || true) ; \
	fi
	@(cd $(BUILDDIR) ; find  -path ./DEBIAN -prune -o -type f -print0 | xargs -0 md5sum > DEBIAN/md5sums )
	@if [ -d DOC ] ; then mkdir $(BUILDDIR)/usr/share/doc/$(PKG) -p ; (cd DOC && tar --dereference -cf - .) | (cd $(BUILDDIR)/usr/share/doc/$(PKG) && tar -xvf -) ; fi
	@chmod -R g-s $(BUILDDIR)
	@fakeroot dpkg-deb -b $(BUILDDIR) $*
	@rm -r $(BUILDDIR)
	@rm -f .upload-$*

NUM_CHANGES:=$(shell find ROOT $$(ls dists/) -mindepth 2 -type f -newer ROOT/DEBIAN/control | wc -l)
# To bump the version
bumpvers: OVERSION:=$(shell grep ^Version: ROOT/DEBIAN/control | awk ' { print $$2 } ')
bumpvers: NVER:=$(word 1, $(subst -ch, ,$(OVERSION)))-ch$(shell echo $$((1+$(word 2, $(subst -ch, ,$(OVERSION))))))
bumpvers:
ifneq ($(NUM_CHANGES),0)
	@echo Bumping deb version of $(PKG) from $(OVERSION) to $(NVER)
	@sed -i 's/^Version: $(OVERSION)/Version: $(NVER)/' ROOT/DEBIAN/control
else
	@echo No deb version bump needed for $(PKG)
endif

# internal usage
gitwarning:
	$(eval CURRENTTAG:=$(shell git tag -l --sort=version:refname | tail -n1))
	@if [ "$(COMMITSAHEAD)" -ne 0 ] || [ "$(UNCOMMITED_CHANGES)" -ne 0 ] ;\
	then \
	echo ;\
	echo "  ============  Have you pushed to git?? ============" ;\
	echo "  You are $(COMMITSAHEAD) commit(s) ahead of upstream" ;\
	if [ "$(UNCOMMITED_CHANGES)" -ne 0 ] ;\
	then \
	echo "  List of uncommitted changes:" ;\
	git status --porcelain | sed "s/^/  /" ;\
	fi ;\
	echo "  ===================================================" ;\
	echo ;\
	fi
	@if [ "$(CURRENTTAG)" != "$(VERSION)" ] ;\
	then \
	echo ;\
	echo "  ============  Please consider tagging!! ============" ;\
	echo "  current tag is $(CURRENTTAG)" ;\
	echo "  after pushing your changes, run:" ;\
	echo "  make gittag" ;\
	echo "  to tag this as $(VERSION) and push the tag to origin" ;\
	echo "  ====================================================" ;\
	echo ;\
	fi

# check if a newer version of the makedeb Makefile is available
# the else .. /bin/true branch is just to suppress the "nothing
# to be done" message that make would otherwise show
selfcheck:
	$(eval MYVERSION:=$(shell (cd $(MAKEDEBDIR); git rev-parse HEAD)))
	$(eval REMOTEVERSION:=$(shell (cd $(MAKEDEBDIR); git ls-remote origin -h refs/heads/master | cut -f 1)))
ifneq ($(MYVERSION),$(REMOTEVERSION))
	@echo "  A newer version of the makedeb repo is available, so you should: cd $(MAKEDEBDIR) \; git pull"
	@/bin/false
else
	@/bin/true
endif

gittag:
	@git tag -a $(VERSION) -m $(VERSION)
	@git push --tags

clean: 	## remove *.deb and .upload*
	@rm -f .upload*
	@find -name \*.deb -delete

.upload-%: DIST=$*
.upload-%:
	@scp $(DIST)/$(call lc,$(PKG))_$(VERSION)_$(ARCH).deb root@$(DOWNLOADS):/tmp/
	@ssh root@$(DOWNLOADS) make -C /var/www/html/local-debs debtodist-$(DIST) DEB=/tmp/$(call lc,$(PKG))_$(VERSION)_$(ARCH).deb
	@touch .upload-$(DIST)

check:
	lintian *.deb

define GITIGNORE
**/*.deb
*.deb
endef

define CONTROLFILE
Package:
Priority: optional
Section:
Maintainer:
Architecture: all
Version: 1.0-ch1
Depends:
Description:
endef

export GITIGNORE
.gitignore:
	echo "$$GITIGNORE" > $@

export CONTROLFILE
ROOT/DEBIAN/control:
	@mkdir -p $(@D)
	@echo "$$CONTROLFILE" > $@

dists:
	@mkdir -p $@

skel: .gitignore ROOT/DEBIAN/control dists	## bootstrap a new repository
	@echo
	@echo Next, you will want to:
	@echo 1. edit ROOT/DEBIAN/control
	@echo 2. touch dists/xenial \(or whichever dists this package should build for\)
	@echo 3. make build \(which will create a deb\)


.PHONY: build clean bumpvers gitwarning selfcheck
